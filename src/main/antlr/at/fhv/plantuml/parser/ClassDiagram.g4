/*
 * Grammar for plantuml class diagrams
 * http://plantuml.com/class-diagram
 * It only supports a subset of the language possibilites
 * see Readme.md for the details
*/
grammar ClassDiagram;

@header {
  package at.fhv.plantuml.parser;
}

classdiagram
    :    START
         NL
         (
              (    classDeclaration
              |    interfacDeclaration
              |    enumDeclaration
              |    packageDeclaration
              |    link
              )
              NL
         )+
         END
    ;

link
    :    leftType=type
         leftCardinality=cardinality?
         linkType
         rightCardinality=cardinality?
         rightType=type
         label?
    ;

label
    :
        COLON
        ALPHANUMERIC+
    ;

cardinality
    :    QUOTATION
         MULTIPLICITY
         QUOTATION
    ;

type
    :    ALPHANUMERIC+
         (
            DOT
            ALPHANUMERIC+
         )*
         (
             (
                ANGLE_BRACKET_OPEN
                type
                (
                    COMMA
                    type
                )?
                ANGLE_BRACKET_CLOSE
             )
             |
             (
                SQUARE_BRACKET_OPEN
                SQUARE_BRACKET_CLOSE
             )
         )?
    ;

linkType
    :    extension
    |    composition
    |    aggregation
    |    association
    ;

extension
    :   EXTENSION_RIGHT
        | EXTENSION_LEFT
    ;

composition
    :   COMPOSITION_RIGHT
        | COMPOSITION_LEFT
    ;

aggregation
    :   AGGREGATION_RIGHT
        | AGGREGATION_LEFT
    ;

association
    :   ASSOCIATION_RIGHT
        | ASSOCIATION_LEFT
    ;

classDeclaration
    :    (   CLASS
         |   ABSTRACT_CLASS
         )
         ALPHANUMERIC+
         generics?
         stereotype?
         block?
    ;

interfacDeclaration
    :    INTERFACE
         ALPHANUMERIC+
         generics?
         stereotype?
         block?
    ;

block
    :    CURLY_BRACKET_OPEN
         NL
             (
                 (   field
                 |   method
                 )
                 NL
             )+
         CURLY_BRACKET_CLOSE
    ;

generics
    :    ANGLE_BRACKET_OPEN
         QUESTION_MARK?
         ALPHANUMERIC+
         ANGLE_BRACKET_CLOSE
    ;

stereotype
    :    ANGLE_BRACKET_OPEN
         ANGLE_BRACKET_OPEN
         ALPHANUMERIC+
         ANGLE_BRACKET_CLOSE
         ANGLE_BRACKET_CLOSE
    ;

field
    :   (   PRIVATE
        |   PROTECTED
        |   PUBLIC
        )
        (   ABSTRACT
        |   STATIC
        )?
        ALPHANUMERIC+
        COLON
        type
    ;

method
    :   (   PRIVATE
        |   PROTECTED
        |   PUBLIC
        )
        (   ABSTRACT
        |   STATIC
        )?
        ALPHANUMERIC+
        METHOD
        (   COLON
            type
            parameters?
        )?
    ;

parameters
    :   CURLY_BRACKET_OPEN
        parameter
        (   COMMA
            parameter
        )*
        CURLY_BRACKET_CLOSE
    ;

parameter
    :   ALPHANUMERIC+
        COLON
        type
    ;

enumDeclaration
    :    ENUM
         ALPHANUMERIC+
         stereotype?
         enumBlock?
    ;

enumBlock
    :    CURLY_BRACKET_OPEN
         NL
         (    enumValue
              NL
         )+
         CURLY_BRACKET_CLOSE
    ;

enumValue
    :   ALPHANUMERIC+
    ;

packageDeclaration
    :   PACKAGE
        packageName
        packageBlock
    ;

packageName
    :   ALPHANUMERIC+
        (   DOT
            ALPHANUMERIC+
        )*
    ;

packageBlock
    :   CURLY_BRACKET_OPEN
        NL
        (
            (    classDeclaration
            |    interfacDeclaration
            |    enumDeclaration
            |    packageDeclaration
            |    link
            )
            NL
        )+
        CURLY_BRACKET_CLOSE
    ;

// start and end markers
START : '@startuml';
END : '@enduml';
NL : ('\r'? '\n' | '\r')+ ;

// links
EXTENSION_LEFT : '<|--';
EXTENSION_RIGHT : '--|>';
COMPOSITION_LEFT : '*--';
COMPOSITION_RIGHT : '--*';
AGGREGATION_LEFT : 'o--';
AGGREGATION_RIGHT : '--o';
ASSOCIATION_LEFT : '<--';
ASSOCIATION_RIGHT : '-->';

// visibility modifiers
PRIVATE : '-';
PROTECTED : '#';
PUBLIC : '+';

// symbols
COLON : ':';
QUESTION_MARK : '?';
COMMA : ',';

// brackets
ANGLE_BRACKET_OPEN : '<';
ANGLE_BRACKET_CLOSE : '>';
CURLY_BRACKET_OPEN : '{';
CURLY_BRACKET_CLOSE : '}';
SQUARE_BRACKET_OPEN : '[';
SQUARE_BRACKET_CLOSE : ']';

// methods
METHOD : '()';

// static and abstract
STATIC : CURLY_BRACKET_OPEN 'static' CURLY_BRACKET_CLOSE;
ABSTRACT : CURLY_BRACKET_OPEN 'abstract' CURLY_BRACKET_CLOSE;

// types
CLASS : 'class';
INTERFACE : 'interface';
ABSTRACT_CLASS : 'abstract class';
ENUM : 'enum';
PACKAGE : 'package';

// cardinality
MULTIPLICITY : (ASTERISK | NUMBER | (NUMBER TO NUMBER) | (NUMBER TO ASTERISK));
TO : DOT DOT;
QUOTATION: '"';
ASTERISK: '*';
DOT : '.';

// basics
fragment WORD: (LOWERCASE | UPPERCASE)+;
ALPHANUMERIC : (WORD | NUMBER)+;
fragment LOWERCASE : [a-z];
fragment UPPERCASE : [A-Z];
fragment NUMBER : DIGIT+;
fragment DIGIT : [0-9];

// skipping
WS : (' ' | '\t') -> skip;