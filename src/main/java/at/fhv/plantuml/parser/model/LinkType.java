package at.fhv.plantuml.parser.model;

public enum LinkType {
    EXTENSION, COMPOSITION, AGGREGATION, ASSOCIATION
}
