package at.fhv.plantuml.parser.model;

import java.util.List;

public class Class {
    private final String name;
    private final boolean isAbstract;
    private final String generic;
    private final String stereotype;
    private final List<Field> fields;
    private final List<Method> methods;
    private final List<String> extensions;

    public Class(String name, boolean isAbstract, String generic, String stereotype, List<Field> fields, List<Method> methods, List<String> extensions) {
        this.name = name;
        this.isAbstract = isAbstract;
        this.generic = generic;
        this.stereotype = stereotype;
        this.fields = fields;
        this.methods = methods;
        this.extensions = extensions;
    }

    public String getName() {
        return name;
    }

    public boolean isAbstract() {
        return isAbstract;
    }

    public String getGeneric() {
        return generic;
    }

    public String getStereotype() {
        return stereotype;
    }

    public List<Field> getFields() {
        return fields;
    }

    public List<Method> getMethods() {
        return methods;
    }

    public List<String> getExtensions() {
        return extensions;
    }
}
