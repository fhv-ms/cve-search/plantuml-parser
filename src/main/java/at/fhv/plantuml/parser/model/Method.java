package at.fhv.plantuml.parser.model;

import java.util.List;

public class Method {
    private final Visibility visibility;
    private final boolean isAbstract;
    private final boolean isStatic;
    private final String name;
    private final String returnType;
    private final List<Parameter> parameters;

    public Method(Visibility visibility, boolean isAbstract, boolean isStatic, String name, String returnType, List<Parameter> parameters) {
        this.visibility = visibility;
        this.isAbstract = isAbstract;
        this.isStatic = isStatic;
        this.name = name;
        this.returnType = returnType;
        this.parameters = parameters;
    }

    public Visibility getVisibility() {
        return visibility;
    }

    public boolean isAbstract() {
        return isAbstract;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public String getName() {
        return name;
    }

    public String getReturnType() {
        return returnType;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }
}
