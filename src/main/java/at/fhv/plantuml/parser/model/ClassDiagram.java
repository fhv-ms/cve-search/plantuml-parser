package at.fhv.plantuml.parser.model;

import java.util.List;

public class ClassDiagram {
    private List<Package> packages;

    public ClassDiagram(List<Package> packages){
        this.packages = packages;
    }

    public List<Package> getPackages() {
        return packages;
    }
}
