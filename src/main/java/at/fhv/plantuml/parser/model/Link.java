package at.fhv.plantuml.parser.model;

public class Link {
    private final String sourceType;
    private final String sourceCardinality;
    private final LinkType type;
    private final String targetCardinality;
    private final String targetType;
    private final String label;

    public Link(String sourceType, String sourceCardinality, LinkType type, String targetCardinality, String targetType, String label) {
        this.sourceType = sourceType;
        this.sourceCardinality = sourceCardinality;
        this.type = type;
        this.targetCardinality = targetCardinality;
        this.targetType = targetType;
        this.label = label;
    }

    public String getSourceType() {
        return sourceType;
    }

    public String getSourceCardinality() {
        return sourceCardinality;
    }

    public LinkType getType() {
        return type;
    }

    public String getTargetCardinality() {
        return targetCardinality;
    }

    public String getTargetType() {
        return targetType;
    }

    public String getLabel() {
        return label;
    }
}
