package at.fhv.plantuml.parser.model;

public enum Visibility {
    PRIVATE, PROTECTED, PUBLIC
}
