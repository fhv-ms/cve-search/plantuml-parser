package at.fhv.plantuml.parser.model;

public class Field {
    private final Visibility visibility;
    private final boolean isAbstract;
    private final boolean isStatic;
    private final String name;
    private final String type;

    public Field(Visibility visibility, boolean isAbstract, boolean isStatic, String name, String type) {
        this.visibility = visibility;
        this.isAbstract = isAbstract;
        this.isStatic = isStatic;
        this.name = name;
        this.type = type;
    }

    public Visibility getVisibility() {
        return visibility;
    }

    public boolean isAbstract() {
        return isAbstract;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }
}
