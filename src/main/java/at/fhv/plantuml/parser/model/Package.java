package at.fhv.plantuml.parser.model;

import java.util.List;

public class Package {
    private final String name;
    private final List<Class> classes;
    private final List<Interface> interfaces;
    private final List<Enum> enums;
    private final List<Package> packages;

    public Package(String name, List<Class> classes, List<Interface> interfaces, List<Enum> enums, List<Package> packages) {
        this.name = name;
        this.classes = classes;
        this.interfaces = interfaces;
        this.enums = enums;
        this.packages = packages;
    }

    public String getName() {
        return name;
    }

    public List<Class> getClasses() {
        return classes;
    }

    public List<Interface> getInterfaces() {
        return interfaces;
    }

    public List<Enum> getEnums() {
        return enums;
    }

    public List<Package> getPackages() {
        return packages;
    }
}
