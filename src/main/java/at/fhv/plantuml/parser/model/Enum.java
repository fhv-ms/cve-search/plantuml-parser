package at.fhv.plantuml.parser.model;

import java.util.List;

public class Enum {
    private final String name;
    private final List<String> values;

    public Enum(String name, List<String> values) {
        this.name = name;
        this.values = values;
    }

    public String getName(){
        return name;
    }

    public List<String> getValues() {
        return values;
    }
}
