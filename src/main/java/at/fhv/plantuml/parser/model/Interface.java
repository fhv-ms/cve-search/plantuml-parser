package at.fhv.plantuml.parser.model;

import java.util.List;

public class Interface {
    private final String name;
    private final String generic;
    private final String stereotype;
    private final List<Field> fields;
    private final List<Method> methods;
    private final List<String> extensions;

    public Interface(String name, String generic, String stereotype, List<Field> fields, List<Method> methods, List<String> extensions) {
        this.name = name;
        this.generic = generic;
        this.stereotype = stereotype;
        this.fields = fields;
        this.methods = methods;
        this.extensions = extensions;
    }

    public String getName() {
        return name;
    }

    public String getGeneric() {
        return generic;
    }

    public String getStereotype() {
        return stereotype;
    }

    public List<Field> getFields() {
        return fields;
    }

    public List<Method> getMethods() {
        return methods;
    }

    public List<String> getExtensions() {
        return extensions;
    }
}
