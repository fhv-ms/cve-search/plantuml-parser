package at.fhv.plantuml.parser.visitor;

import at.fhv.plantuml.parser.ClassDiagramBaseVisitor;
import at.fhv.plantuml.parser.ClassDiagramParser;
import at.fhv.plantuml.parser.model.Link;
import at.fhv.plantuml.parser.model.LinkType;

import java.util.Arrays;
import java.util.stream.Collectors;

public class LinkVisitor extends AbstractMemberVisitor<Link> {

    @Override
    public Link visitLink(ClassDiagramParser.LinkContext ctx) {
        LinkType linkType = getLinkType(ctx.linkType());
        String label = (ctx.label() != null) ? ctx.label().ALPHANUMERIC().stream().map(n -> n.toString()).collect(Collectors.joining()) : "";
        String[] sourceAndTarget = getCorrectSourceAndTarget(ctx);

        return new Link(sourceAndTarget[0], sourceAndTarget[1], linkType, sourceAndTarget[2], sourceAndTarget[3], label);
    }

    private LinkType getLinkType(ClassDiagramParser.LinkTypeContext ctx){
        if(ctx.extension() != null){
            return LinkType.EXTENSION;
        }

        if(ctx.composition() != null){
            return LinkType.COMPOSITION;
        }

        if(ctx.aggregation() != null){
            return LinkType.AGGREGATION;
        }

        if(ctx.association() != null){
            return LinkType.ASSOCIATION;
        }

        // cannot happen as it would lead to a parser error
        return null;
    }

    /**
     * Get the correct order of the source and target as they might be not correct in the parsed grammer
     * @param ctx The link context
     * @return The correct order: 0: source type, 1: source cardinality, 2: target cardinality, 3: target type
     */
    private String[] getCorrectSourceAndTarget(ClassDiagramParser.LinkContext ctx){
        String sourceType = getType(ctx.leftType);
        String sourceCardinality = (ctx.leftCardinality != null) ? ctx.leftCardinality.MULTIPLICITY().toString() : "";
        String targetType = getType(ctx.rightType);
        String targetCardinality = (ctx.rightCardinality != null) ? ctx.rightCardinality.MULTIPLICITY().toString() : "";

        if((ctx.linkType().extension() != null && ctx.linkType().extension().EXTENSION_LEFT() != null)
                || (ctx.linkType().composition() != null && ctx.linkType().composition().COMPOSITION_LEFT() != null)
                || (ctx.linkType().aggregation() != null && ctx.linkType().aggregation().AGGREGATION_LEFT() != null)
                || (ctx.linkType().association() != null && ctx.linkType().association().ASSOCIATION_LEFT() != null)
        ) {
            String tmp = sourceType;
            sourceType = targetType;
            targetType = tmp;
            tmp = sourceCardinality;
            sourceCardinality = targetCardinality;
            targetCardinality = tmp;
        }

        return new String[]{sourceType, sourceCardinality, targetType, targetCardinality};
    }
}
