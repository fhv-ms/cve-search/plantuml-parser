package at.fhv.plantuml.parser.visitor;

import at.fhv.plantuml.parser.ClassDiagramParser;
import at.fhv.plantuml.parser.model.Parameter;

import java.util.stream.Collectors;

public class ParameterVisitor extends AbstractMemberVisitor<Parameter> {

    @Override
    public Parameter visitParameter(ClassDiagramParser.ParameterContext ctx) {
        String name = ctx.ALPHANUMERIC().stream().map(n -> n.toString()).collect(Collectors.joining());
        String type = getType(ctx.type());
        return new Parameter(name, type);
    }
}
