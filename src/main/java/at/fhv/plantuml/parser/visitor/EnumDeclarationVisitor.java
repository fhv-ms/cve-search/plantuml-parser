package at.fhv.plantuml.parser.visitor;

import at.fhv.plantuml.parser.ClassDiagramBaseVisitor;
import at.fhv.plantuml.parser.ClassDiagramParser;
import at.fhv.plantuml.parser.model.Enum;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class EnumDeclarationVisitor extends ClassDiagramBaseVisitor<Enum> {

    @Override
    public Enum visitEnumDeclaration(ClassDiagramParser.EnumDeclarationContext ctx) {
        String name = ctx.ALPHANUMERIC().stream().map(n -> n.toString()).collect(Collectors.joining());
        List<String> values = getValues(ctx);
        return new Enum(name, values);
    }

    private List<String> getValues(ClassDiagramParser.EnumDeclarationContext ctx){
        if(ctx.enumBlock() == null){
            return Collections.emptyList();
        }

        return ctx.enumBlock().enumValue()
                .stream()
                .map(enumValue -> enumValue.getText())
                .collect(toList());
    }
}
