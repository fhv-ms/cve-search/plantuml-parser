package at.fhv.plantuml.parser.visitor;

import at.fhv.plantuml.parser.ClassDiagramParser;
import at.fhv.plantuml.parser.model.Field;
import at.fhv.plantuml.parser.model.Visibility;

import java.util.stream.Collectors;

public class FieldVisitor extends AbstractMemberVisitor<Field> {

    @Override
    public Field visitField(ClassDiagramParser.FieldContext ctx) {
        Visibility visibility = null;
        if(ctx.PUBLIC() != null){
            visibility = Visibility.PUBLIC;
        } else if (ctx.PROTECTED() != null){
            visibility = Visibility.PROTECTED;
        } else if (ctx.PRIVATE() != null){
            visibility = Visibility.PRIVATE;
        }

        boolean isAbstract = ctx.ABSTRACT() != null;
        boolean isStatic = ctx.STATIC() != null;

        String name = ctx.ALPHANUMERIC().stream().map(n -> n.toString()).collect(Collectors.joining());
        String type = getType(ctx.type());

        return new Field(visibility, isAbstract, isStatic, name, type);
    }
}
