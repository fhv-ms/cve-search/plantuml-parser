package at.fhv.plantuml.parser.visitor;

import at.fhv.plantuml.parser.ClassDiagramBaseVisitor;
import at.fhv.plantuml.parser.ClassDiagramParser;

import java.util.List;

public abstract class AbstractMemberVisitor<T> extends ClassDiagramBaseVisitor<T> {

    protected String getType(ClassDiagramParser.TypeContext ctx) {
        if(ctx == null){
            return "void";
        }

        StringBuilder type = new StringBuilder();
        type.append(getFullQualifiedName(ctx));

        if(ctx.SQUARE_BRACKET_OPEN() != null){
            type.append(ctx.SQUARE_BRACKET_OPEN());
            type.append(ctx.SQUARE_BRACKET_CLOSE());
        }

        if(ctx.ANGLE_BRACKET_OPEN() != null){
            type.append(ctx.ANGLE_BRACKET_OPEN());
            List<ClassDiagramParser.TypeContext> types = ctx.type();
            for (int i = 0; i < types.size(); i++) {
                if(i > 0){
                    type.append(ctx.COMMA()).append(" ");
                }

                type.append(getType(types.get(i)));
            }
            type.append(ctx.ANGLE_BRACKET_CLOSE());
        }

        return type.toString();
    }

    private String getFullQualifiedName(ClassDiagramParser.TypeContext ctx){
        StringBuilder type = new StringBuilder();
        for (int i = 0; i < ctx.ALPHANUMERIC().size(); i++) {
            if(i > 0){
                type.append(ctx.DOT(i-1));
            }

            type.append(ctx.ALPHANUMERIC(i));
        }
        return type.toString();
    }
}
