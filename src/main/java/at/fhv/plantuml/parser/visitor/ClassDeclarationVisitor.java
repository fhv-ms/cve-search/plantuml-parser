package at.fhv.plantuml.parser.visitor;

import at.fhv.plantuml.parser.ClassDiagramBaseVisitor;
import at.fhv.plantuml.parser.ClassDiagramParser;
import at.fhv.plantuml.parser.model.Class;
import at.fhv.plantuml.parser.model.Field;
import at.fhv.plantuml.parser.model.Link;
import at.fhv.plantuml.parser.model.Method;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class ClassDeclarationVisitor extends ClassDiagramBaseVisitor<Class> {

    @Override
    public Class visitClassDeclaration(ClassDiagramParser.ClassDeclarationContext ctx) {
        String name = ctx.ALPHANUMERIC().stream().map(n -> n.toString()).collect(Collectors.joining());

        boolean isAbstract = ctx.ABSTRACT_CLASS() != null;

        String generic = (ctx.generics() != null) ? ctx.generics().getText().substring(1,
                ctx.generics().getText().length() - 1) : "";

        String stereotype = (ctx.stereotype() != null) ? ctx.stereotype().getText().substring(1,
                ctx.stereotype().getText().length() - 1) : "";

        List<Field> fields = getFields(ctx);
        List<Method> methods = getMethods(ctx);

        return new Class(name, isAbstract, generic, stereotype, fields, methods, new ArrayList<>());
    }

    private List<Field> getFields(ClassDiagramParser.ClassDeclarationContext ctx){
        if(ctx.block() == null){
            return Collections.emptyList();
        }

        return ctx.block().field()
                .stream()
                .map(field -> field.accept(new FieldVisitor()))
                .collect(toList());
    }

    private List<Method> getMethods(ClassDiagramParser.ClassDeclarationContext ctx){
        if(ctx.block() == null){
            return Collections.emptyList();
        }

        return ctx.block().method()
                .stream()
                .map(method -> method.accept(new MethodVisitor()))
                .collect(toList());
    }
}
