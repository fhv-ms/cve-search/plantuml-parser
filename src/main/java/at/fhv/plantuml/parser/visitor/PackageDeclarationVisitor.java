package at.fhv.plantuml.parser.visitor;

import at.fhv.plantuml.parser.ClassDiagramBaseVisitor;
import at.fhv.plantuml.parser.ClassDiagramParser;
import at.fhv.plantuml.parser.model.Class;
import at.fhv.plantuml.parser.model.Enum;
import at.fhv.plantuml.parser.model.Interface;
import at.fhv.plantuml.parser.model.Package;

import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class PackageDeclarationVisitor extends ClassDiagramBaseVisitor<Package> {

    @Override
    public Package visitPackageDeclaration(ClassDiagramParser.PackageDeclarationContext ctx) {
        String name = getName(ctx.packageName());

        List<Class> classes = getClasses(ctx);
        List<Interface> interfaces = getInterfaces(ctx);
        List<Enum> enums = getEnums(ctx);
        List<Package> packages = getPackages(ctx);

        return new Package(name, classes, interfaces, enums, packages);
    }

    private String getName(ClassDiagramParser.PackageNameContext ctx){
        StringBuilder type = new StringBuilder();
        for (int i = 0; i < ctx.ALPHANUMERIC().size(); i++) {
            if(i > 0){
                type.append(ctx.DOT(i-1));
            }

            type.append(ctx.ALPHANUMERIC(i));
        }
        return type.toString();
    }

    private List<Class> getClasses(ClassDiagramParser.PackageDeclarationContext ctx){
        if(ctx.packageBlock() == null){
            return Collections.emptyList();
        }

        return ctx.packageBlock().classDeclaration()
                .stream()
                .map(method -> method.accept(new ClassDeclarationVisitor()))
                .collect(toList());
    }

    private List<Interface> getInterfaces(ClassDiagramParser.PackageDeclarationContext ctx){
        if(ctx.packageBlock() == null){
            return Collections.emptyList();
        }

        return ctx.packageBlock().interfacDeclaration()
                .stream()
                .map(method -> method.accept(new InterfaceDeclarationVisitor()))
                .collect(toList());
    }

    private List<Enum> getEnums(ClassDiagramParser.PackageDeclarationContext ctx){
        if(ctx.packageBlock() == null){
            return Collections.emptyList();
        }

        return ctx.packageBlock().enumDeclaration()
                .stream()
                .map(method -> method.accept(new EnumDeclarationVisitor()))
                .collect(toList());
    }

    private List<Package> getPackages(ClassDiagramParser.PackageDeclarationContext ctx){
        if(ctx.packageBlock() == null){
            return Collections.emptyList();
        }

        return ctx.packageBlock().packageDeclaration()
                .stream()
                .map(method -> method.accept(new PackageDeclarationVisitor()))
                .collect(toList());
    }
}
