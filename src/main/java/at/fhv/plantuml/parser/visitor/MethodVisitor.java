package at.fhv.plantuml.parser.visitor;

import at.fhv.plantuml.parser.ClassDiagramParser;
import at.fhv.plantuml.parser.model.Method;
import at.fhv.plantuml.parser.model.Parameter;
import at.fhv.plantuml.parser.model.Visibility;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class MethodVisitor extends AbstractMemberVisitor<Method> {

    @Override
    public Method visitMethod(ClassDiagramParser.MethodContext ctx) {
        Visibility visibility = null;
        if(ctx.PUBLIC() != null){
            visibility = Visibility.PUBLIC;
        } else if (ctx.PROTECTED() != null){
            visibility = Visibility.PROTECTED;
        } else if (ctx.PRIVATE() != null){
            visibility = Visibility.PRIVATE;
        }

        boolean isAbstract = ctx.ABSTRACT() != null;
        boolean isStatic = ctx.STATIC() != null;

        String name = ctx.ALPHANUMERIC().stream().map(n -> n.toString()).collect(Collectors.joining());
        String type = getType(ctx.type());
        List<Parameter> parameters = getParameters(ctx);

        return new Method(visibility, isAbstract, isStatic, name, type, parameters);
    }

    private List<Parameter> getParameters(ClassDiagramParser.MethodContext ctx){
        if(ctx.parameters() == null){
            return Collections.emptyList();
        }

        return ctx.parameters().parameter()
                .stream()
                .map(parameter -> parameter.accept(new ParameterVisitor()))
                .collect(toList());
    }
}
