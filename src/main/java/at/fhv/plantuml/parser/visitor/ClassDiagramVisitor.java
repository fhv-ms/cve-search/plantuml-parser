package at.fhv.plantuml.parser.visitor;

import at.fhv.plantuml.parser.ClassDiagramBaseVisitor;
import at.fhv.plantuml.parser.ClassDiagramParser;
import at.fhv.plantuml.parser.model.*;
import at.fhv.plantuml.parser.model.Class;
import at.fhv.plantuml.parser.model.Enum;
import at.fhv.plantuml.parser.model.Package;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class ClassDiagramVisitor extends ClassDiagramBaseVisitor<ClassDiagram> {

    @Override
    public ClassDiagram visitClassdiagram(ClassDiagramParser.ClassdiagramContext ctx) {
        List<Class> classes = getClasses(ctx);
        List<Interface> interfaces = getInterfaces(ctx);
        List<Package> packages = getPackages(ctx);
        List<Enum> enums = getEnums(ctx);
        List<Link> links = getLinks(ctx);
        return buildClassDiagram(classes, interfaces, enums, packages, links);
    }

    private ClassDiagram buildClassDiagram(List<Class> classes, List<Interface> interfaces, List<Enum> enums,
                                           List<Package> packages, List<Link> links) {
        // create classes for links
        // TODO implement
        // Association, Composition, Aggregation: Generate field according to multiplicity (simple object, list, etc)
        // Extension: Add info to class -> used for realizing interfaces/classes (generalization)

        // classes, interfaces and links on this level belong to the default package, therefor we create it
        Package defaultPackage = new Package("", classes, interfaces, enums, new ArrayList<>());
        packages.add(defaultPackage);

        return new ClassDiagram(packages);
    }

    private List<Class> getClasses(ClassDiagramParser.ClassdiagramContext ctx){
        if(ctx.classDeclaration() == null){
            return Collections.emptyList();
        }

        return ctx.classDeclaration()
                .stream()
                .map(classDeclaration -> classDeclaration.accept(new ClassDeclarationVisitor()))
                .collect(toList());
    }

    private List<Interface> getInterfaces(ClassDiagramParser.ClassdiagramContext ctx){
        if(ctx.interfacDeclaration() == null){
            return Collections.emptyList();
        }

        return ctx.interfacDeclaration()
                .stream()
                .map(interfaceDeclaration -> interfaceDeclaration.accept(new InterfaceDeclarationVisitor()))
                .collect(toList());
    }

    private List<Package> getPackages(ClassDiagramParser.ClassdiagramContext ctx){
        if(ctx.packageDeclaration() == null){
            return Collections.emptyList();
        }

        return ctx.packageDeclaration()
                .stream()
                .map(packageDeclaration -> packageDeclaration.accept(new PackageDeclarationVisitor()))
                .collect(toList());
    }

    private List<Enum> getEnums(ClassDiagramParser.ClassdiagramContext ctx){
        if(ctx.enumDeclaration() == null){
            return Collections.emptyList();
        }

        return ctx.enumDeclaration()
                .stream()
                .map(enumDeclaration -> enumDeclaration.accept(new EnumDeclarationVisitor()))
                .collect(toList());
    }

    private List<Link> getLinks(ClassDiagramParser.ClassdiagramContext ctx){
        if(ctx.link() == null){
            return Collections.emptyList();
        }

        return ctx.link()
                .stream()
                .map(link -> link.accept(new LinkVisitor()))
                .collect(toList());
    }
}
