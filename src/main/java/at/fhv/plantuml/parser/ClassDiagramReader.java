package at.fhv.plantuml.parser;

import at.fhv.plantuml.parser.model.ClassDiagram;
import at.fhv.plantuml.parser.visitor.ClassDiagramVisitor;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.IOException;

/**
 * The ClassDiagramReader converts a PlantUML classdiagram file to a Java Object tree
 */
public class ClassDiagramReader {

    /**
     * Read the given PlantUML class diagram file and return the java object tree
     * @param file The file to read
     * @return The java object tree of the given class diagram
     */
    public static ClassDiagram read(String file) throws IOException {
        ClassDiagramLexer lexer = new ClassDiagramLexer(CharStreams.fromFileName(file));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ClassDiagramParser parser = new ClassDiagramParser(tokens);

        ClassDiagramVisitor classDiagramVisitor = new ClassDiagramVisitor();
        return classDiagramVisitor.visitClassdiagram(parser.classdiagram());
    }

}
