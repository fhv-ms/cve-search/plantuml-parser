package at.fhv.plantuml.parser;

import at.fhv.plantuml.parser.model.*;
import at.fhv.plantuml.parser.model.Class;
import at.fhv.plantuml.parser.model.Enum;
import at.fhv.plantuml.parser.model.Package;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URL;
import java.nio.file.Paths;
import java.util.List;

/**
 * Test the class diagram reader
 */
public class ClassDiagramReaderTest {

    /**
     * Read the given file and return the parsed class diagram object
     * @param file The file to load
     * @return the parsed class diagram object
     */
    private ClassDiagram read(String file) throws Exception {
        URL resource = getClass().getResource("/" + file);
        File f = Paths.get(resource.toURI()).toFile();
        return ClassDiagramReader.read(f.getAbsolutePath());
    }

    /**
     * Test for abstract and static fields and methods
     */
    @Test
    public void testAbstractStatic() throws Exception {
        ClassDiagram d = read("abstract_static.plantuml");

        List<Package> packages = d.getPackages();
        Assertions.assertEquals(1, packages.size());

        Package p = d.getPackages().get(0);
        validatePackage(p, "", 1, 0, 0, 0);

        Class c = p.getClasses().get(0);
        validateClass(c, "Dummy", false, "", "", 1, 1, 0);

        Field f = c.getFields().get(0);
        validateField(f, Visibility.PRIVATE, false, true, "id", "String");

        Method m = c.getMethods().get(0);
        validateMethod(m, Visibility.PUBLIC, true, false, "methods", "void", 0);
    }

    /**
     * Test for fields
     */
    @Test
    public void testFields() throws Exception {
        ClassDiagram d = read("fields.plantuml");

        List<Package> packages = d.getPackages();
        Assertions.assertEquals(1, packages.size());

        Package p = d.getPackages().get(0);
        validatePackage(p, "", 1, 0, 0, 0);

        Class c = p.getClasses().get(0);
        validateClass(c, "Dummy", false, "", "", 8, 0, 0);

        List<Field> fields = c.getFields();
        validateField(fields.get(0), Visibility.PRIVATE, false, false, "field1", "String");
        validateField(fields.get(1), Visibility.PROTECTED, false, false, "field2", "at.fhv.Object");
        validateField(fields.get(2), Visibility.PUBLIC, false, false, "field3", "List<Object>");
        validateField(fields.get(3), Visibility.PUBLIC, false, false, "field4", "Map<Key, Value>");
        validateField(fields.get(4), Visibility.PUBLIC, false, false, "field5", "String[]");
        validateField(fields.get(5), Visibility.PUBLIC, false, false, "field6", "List<String[]>");
        validateField(fields.get(6), Visibility.PUBLIC, false, false, "field7", "List<List<String>>");
        validateField(fields.get(7), Visibility.PUBLIC, false, false, "field8", "Map<Key, List<String>>");
    }

    /**
     * Test for generics
     */
    @Test
    public void testGenerics() throws Exception {
        ClassDiagram d = read("generics.plantuml");

        List<Package> packages = d.getPackages();
        Assertions.assertEquals(1, packages.size());

        Package p = d.getPackages().get(0);
        validatePackage(p, "", 2, 0, 0, 0);

        Class foo = p.getClasses().get(0);
        validateClass(foo, "Foo", false, "? extends Element", "", 1, 1, 0);
        List<Field> fooFields = foo.getFields();
        validateField(fooFields.get(0), Visibility.PUBLIC, false, false, "element", "Element");
        List<Method> fooMethods = foo.getMethods();
        validateMethod(fooMethods.get(0), Visibility.PRIVATE, false, false, "size", "int", 0);

        Class element = p.getClasses().get(1);
        validateClass(element, "Element", false, "", "", 0, 0, 0);
    }

    /**
     * Test for labels
     */
    @Test
    public void testLabels() throws Exception {
        ClassDiagram d = read("labels.plantuml");

        List<Package> packages = d.getPackages();
        Assertions.assertEquals(1, packages.size());

        Package p = d.getPackages().get(0);
        validatePackage(p, "", 14, 0, 0, 0);

        Class class01 = p.getClasses().get(0);
        validateClass(class01, "Class01", false, "", "", 0, 0, 0);

        Class class02 = p.getClasses().get(1);
        validateClass(class02, "Class02", false, "", "", 1, 0, 0);
        List<Field> class02Fields = class02.getFields();
        validateField(class02Fields.get(0), Visibility.PUBLIC, false, false, "class01", "Class01");

        Class class03 = p.getClasses().get(2);
        validateClass(class03, "Class03", false, "", "", 0, 0, 0);

        Class class04 = p.getClasses().get(3);
        validateClass(class04, "Class04", false, "", "", 1, 0, 0);
        List<Field> class04Fields = class04.getFields();
        validateField(class04Fields.get(0), Visibility.PUBLIC, false, false, "class03", "Class03");

        Class class05 = p.getClasses().get(4);
        validateClass(class05, "Class05", false, "", "", 1, 0, 0);
        List<Field> class05Fields = class05.getFields();
        validateField(class05Fields.get(0), Visibility.PUBLIC, false, false, "class06", "Class06");

        Class class06 = p.getClasses().get(5);
        validateClass(class06, "Class06", false, "", "", 0, 0, 0);

        Class class07 = p.getClasses().get(6);
        validateClass(class07, "Class07", false, "", "", 0, 0, 0);

        Class class08 = p.getClasses().get(7);
        validateClass(class08, "Class08", false, "", "", 1, 0, 0);
        List<Field> class08Fields = class08.getFields();
        validateField(class08Fields.get(0), Visibility.PUBLIC, false, false, "class07", "Class07");

        Class class09 = p.getClasses().get(8);
        validateClass(class09, "Class09", false, "", "", 0, 0, 0);

        Class class10 = p.getClasses().get(9);
        validateClass(class10, "Class10", false, "", "", 1, 0, 0);
        List<Field> class10Fields = class10.getFields();
        validateField(class10Fields.get(0), Visibility.PUBLIC, false, false, "class09", "Class09");

        Class class11 = p.getClasses().get(10);
        validateClass(class11, "Class11", false, "", "", 0, 0, 0);

        Class class12 = p.getClasses().get(11);
        validateClass(class12, "Class12", false, "", "", 1, 0, 0);
        List<Field> class12Fields = class12.getFields();
        validateField(class12Fields.get(0), Visibility.PUBLIC, false, false, "class10", "Class10");

        Class class13 = p.getClasses().get(12);
        validateClass(class13, "Class13", false, "", "", 0, 0, 0);

        Class class14 = p.getClasses().get(13);
        validateClass(class14, "Class12", false, "", "", 1, 0, 0);
        List<Field> class14Fields = class14.getFields();
        validateField(class14Fields.get(0), Visibility.PUBLIC, false, false, "class13", "Class13");
    }

    /**
     * Test for methods
     */
    @Test
    public void testMethods() throws Exception {
        ClassDiagram d = read("methods.plantuml");

        List<Package> packages = d.getPackages();
        Assertions.assertEquals(1, packages.size());

        Package p = d.getPackages().get(0);
        validatePackage(p, "", 1, 0, 0, 0);

        Class c = p.getClasses().get(0);
        validateClass(c, "Dummy", false, "", "", 0, 7, 0);

        List<Method> methods = c.getMethods();
        validateMethod(methods.get(0), Visibility.PRIVATE, false, false, "method1", "String", 0);
        validateMethod(methods.get(1), Visibility.PUBLIC, false, false, "method2", "String", 2);
        validateParameter(methods.get(1).getParameters().get(0), "arg1", "Foo");
        validateParameter(methods.get(1).getParameters().get(1), "arg2", "bar");
        validateMethod(methods.get(2), Visibility.PROTECTED, false, false, "method3", "at.fhv.Object", 1);
        validateParameter(methods.get(2).getParameters().get(0), "arg1", "Bar");
        validateMethod(methods.get(3), Visibility.PRIVATE, false, false, "method4", "void", 0);
        validateMethod(methods.get(4), Visibility.PUBLIC, false, false, "method5", "void", 1);
        validateParameter(methods.get(4).getParameters().get(0), "arg1", "at.fhv.Object");
        validateMethod(methods.get(5), Visibility.PROTECTED, false, false, "method6", "List<Object>", 1);
        validateParameter(methods.get(5).getParameters().get(0), "arg1", "List<Object>");
        validateMethod(methods.get(6), Visibility.PRIVATE, false, false, "method7", "Map<Key, Value>", 1);
        validateParameter(methods.get(6).getParameters().get(0), "arg1", "Map<Key, Value>");
    }

    /**
     * Test for packages
     */
    @Test
    public void testPackages() throws Exception {
        ClassDiagram d =  read("packages.plantuml");

        List<Package> packages = d.getPackages();
        Assertions.assertEquals(3, packages.size());

        Package p = d.getPackages().get(0);
        validatePackage(p, "at.fhv.test", 2, 0, 0, 0);
        validateClass(p.getClasses().get(0), "Object", false, "", "", 0, 0, 0);
        validateClass(p.getClasses().get(1), "ArrayList", false, "", "", 0, 0, 1);
        validateExtension(p.getClasses().get(1).getExtensions().get(0), "Object");

        Package p1 = d.getPackages().get(1);
        validatePackage(p1, "net.sourceforge.plantuml", 2, 0, 0, 0);
        validateClass(p1.getClasses().get(0), "Object", false, "", "", 0, 0, 0);
        validateClass(p1.getClasses().get(1), "Demo1", false, "", "", 0, 0, 1);
        validateExtension(p.getClasses().get(1).getExtensions().get(0), "Object");
        validateClass(p1.getClasses().get(2), "Demo2", false, "", "", 1, 0, 0);
        validateField(p.getClasses().get(2).getFields().get(0), Visibility.PUBLIC, false, false, "demo1", "Demo1");

        Package p2 = d.getPackages().get(2);
        validatePackage(p2, "at.fhv", 0, 0, 0, 2);
        validatePackage(p2.getPackages().get(0), "entity", 1, 0, 0, 0);
        validateClass(p2.getPackages().get(0).getClasses().get(0), "Foo", false, "", "", 0, 0, 0);
        validatePackage(p2.getPackages().get(1), "webservice", 1, 0, 0, 0);
        validateClass(p2.getPackages().get(1).getClasses().get(0), "Bar", false, "", "", 0, 0, 0);
    }

    /**
     * Test for relations
     */
    @Test
    public void testRelations() throws Exception {
        ClassDiagram d = read("relations.plantuml");

        List<Package> packages = d.getPackages();
        Assertions.assertEquals(1, packages.size());

        Package p = d.getPackages().get(0);
        validatePackage(p, "", 16, 0, 0, 0);

        Class class01 = p.getClasses().get(0);
        validateClass(class01, "Class01", false, "", "", 0, 0, 0);

        Class class02 = p.getClasses().get(1);
        validateClass(class02, "Class02", false, "", "", 0, 0, 1);
        validateExtension(class02.getExtensions().get(1), "Class01");

        Class class03 = p.getClasses().get(2);
        validateClass(class03, "Class03", false, "", "", 0, 0, 1);
        validateExtension(class03.getExtensions().get(1), "Class04");

        Class class04 = p.getClasses().get(3);
        validateClass(class04, "Class04", false, "", "", 0, 0, 0);

        Class class05 = p.getClasses().get(4);
        validateClass(class05, "Class05", false, "", "", 0, 0, 0);

        Class class06 = p.getClasses().get(5);
        validateClass(class06, "Class06", false, "", "", 1, 0, 0);
        validateField(class06.getFields().get(0), Visibility.PUBLIC, false, false, "class05", "Class05");

        Class class07 = p.getClasses().get(6);
        validateClass(class07, "Class07", false, "", "", 1, 0, 0);
        validateField(class07.getFields().get(0), Visibility.PUBLIC, false, false, "class08", "Class08");

        Class class08 = p.getClasses().get(7);
        validateClass(class08, "Class08", false, "", "", 0, 0, 0);

        Class class09 = p.getClasses().get(8);
        validateClass(class09, "Class09", false, "", "", 0, 0, 0);

        Class class10 = p.getClasses().get(9);
        validateClass(class10, "Class10", false, "", "", 1, 0, 0);
        validateField(class10.getFields().get(0), Visibility.PUBLIC, false, false, "class09", "Class09");

        Class class11 = p.getClasses().get(10);
        validateClass(class11, "Class11", false, "", "", 1, 0, 0);
        validateField(class11.getFields().get(0), Visibility.PUBLIC, false, false, "class12", "Class12");

        Class class12 = p.getClasses().get(11);
        validateClass(class12, "Class12", false, "", "", 0, 0, 0);

        Class class13 = p.getClasses().get(12);
        validateClass(class13, "Class13", false, "", "", 0, 0, 0);

        Class class14 = p.getClasses().get(13);
        validateClass(class14, "Class14", false, "", "", 1, 0, 0);
        validateField(class14.getFields().get(0), Visibility.PUBLIC, false, false, "class13", "Class13");

        Class class15 = p.getClasses().get(14);
        validateClass(class15, "Class15", false, "", "", 1, 0, 0);
        validateField(class15.getFields().get(0), Visibility.PUBLIC, false, false, "class16", "Class16");

        Class class16 = p.getClasses().get(15);
        validateClass(class16, "Class16", false, "", "", 0, 0, 0);
    }

    /**
     * Test for stereotypes
     */
    @Test
    public void testStereotypes() throws Exception {
        ClassDiagram d = read("stereotypes.plantuml");

        List<Package> packages = d.getPackages();
        Assertions.assertEquals(1, packages.size());

        Package p = d.getPackages().get(0);
        validatePackage(p, "", 2, 0, 0, 0);

        Class object = p.getClasses().get(0);
        validateClass(object, "Object", false, "", "general", 0, 0, 0);

        Class arrayList = p.getClasses().get(1);
        validateClass(arrayList, "ArrayList", false, "", "", 1, 0, 0);
        List<Field> arrayListFields = arrayList.getFields();
        validateField(arrayListFields.get(0), Visibility.PUBLIC, false, false, "object", "Object");
    }

    /**
     * Test for types
     */
    @Test
    public void testTypes() throws Exception {
        ClassDiagram d = read("types.plantuml");

        List<Package> packages = d.getPackages();
        Assertions.assertEquals(1, packages.size());

        Package p = d.getPackages().get(0);
        validatePackage(p, "", 5, 2, 1, 0);

        Class abstractList = p.getClasses().get(0);
        validateClass(abstractList, "AbstractList", true, "", "", 0, 0, 1);
        validateExtension(abstractList.getExtensions().get(0), "List");

        Interface list = p.getInterfaces().get(0);
        validateInterface(list, "List", "", "", 0, 0, 1);
        validateExtension(list.getExtensions().get(0), "Collection");

        Interface collection = p.getInterfaces().get(1);
        validateInterface(collection, "Collection", "", "", 0, 0, 0);

        Class abstractCollection = p.getClasses().get(1);
        validateClass(abstractCollection, "AbstractCollection", false, "", "", 0, 0, 1);
        validateExtension(abstractCollection.getExtensions().get(0), "Collection");

        Class arrayList = p.getClasses().get(1);
        validateClass(arrayList, "ArrayList", false, "", "", 1, 1, 1);
        validateExtension(arrayList.getExtensions().get(0), "AbstractList");
        validateField(arrayList.getFields().get(0), Visibility.PRIVATE, false, false, "elementData", "at.fhv.Object[]");
        validateMethod(arrayList.getMethods().get(0), Visibility.PRIVATE, false, false, "size", "void", 0);

        Enum timeUnit = p.getEnums().get(0);
        validateEnum(timeUnit, "TimeUnit", 3);
        validateEnumValue(timeUnit.getValues().get(0), "DAYS");
        validateEnumValue(timeUnit.getValues().get(1), "HOURS");
        validateEnumValue(timeUnit.getValues().get(2), "MINUTES");
    }

    /**
     * Validate the given package
     *
     * @param p The package to validate
     * @param expectedName The expected name of the package
     * @param expectedClasses The expected number of classes within the package
     * @param expectedInterfaces The expected number of interfaces within the package
     * @param expectedEnums The expected number of enums within the package
     * @param expectedPackages The expected number of packages within the package
     */
    private void validatePackage(Package p, String expectedName, int expectedClasses, int expectedInterfaces,
                                 int expectedEnums, int expectedPackages){
        Assertions.assertEquals(expectedName, p.getName());
        Assertions.assertEquals(expectedClasses, p.getClasses().size());
        Assertions.assertEquals(expectedInterfaces, p.getInterfaces().size());
        Assertions.assertEquals(expectedEnums, p.getEnums().size());
        Assertions.assertEquals(expectedPackages, p.getPackages().size());
    }

    /**
     * Validate the given class
     *
     * @param c The class to validate
     * @param expectedName The expected name of the class
     * @param expectedIsAbstract The expected value for isAbstract of the class
     * @param expectedGeneric The expected generic of the class
     * @param expectedStereotype The expected stereotype of the class
     * @param expectedFields The expected number of fields within the class
     * @param expectedMethods The expected number of methods within the class
     * @param expectedExtensions The expected number of extensions within the class
     */
    private void validateClass(Class c, String expectedName, boolean expectedIsAbstract, String expectedGeneric,
                               String expectedStereotype, int expectedFields, int expectedMethods, int expectedExtensions){
        Assertions.assertEquals(expectedName, c.getName());
        Assertions.assertEquals(expectedIsAbstract, c.isAbstract());
        Assertions.assertEquals(expectedGeneric, c.getGeneric());
        Assertions.assertEquals(expectedStereotype, c.getStereotype());
        Assertions.assertEquals(expectedFields, c.getFields().size());
        Assertions.assertEquals(expectedMethods, c.getMethods().size());
        Assertions.assertEquals(expectedExtensions, c.getExtensions().size());
    }

    /**
     * Validate the given field
     *
     * @param f The field to validate
     * @param expectedVisibility The expected visibility of the class
     * @param expectedIsAbstract The expected isAbstract of the class
     * @param expectedIsStatic The expected isStatic of the class
     * @param expectedName The expected name of the class
     * @param expectedType The expected type of the class
     */
    private void validateField(Field f, Visibility expectedVisibility, boolean expectedIsAbstract,
                               boolean expectedIsStatic, String expectedName, String expectedType){
        Assertions.assertEquals(expectedVisibility, f.getVisibility());
        Assertions.assertEquals(expectedIsAbstract, f.isAbstract());
        Assertions.assertEquals(expectedIsStatic, f.isStatic());
        Assertions.assertEquals(expectedName, f.getName());
        Assertions.assertEquals(expectedType, f.getType());
    }

    /**
     * Validate the given method
     *
     * @param m The method to validate
     * @param expectedVisibility The expected visibility of the method
     * @param expectedIsAbstract The expected isAbstract of the method
     * @param expectedIsStatic The expected isStatic of the method
     * @param expectedName The expected name of the method
     * @param expectedReturnType The expected type of the method
     * @param expectedParameters The expected number of parameters of the methods
     */
    private void validateMethod(Method m, Visibility expectedVisibility, boolean expectedIsAbstract,
                                boolean expectedIsStatic, String expectedName, String expectedReturnType,
                                int expectedParameters){
        Assertions.assertEquals(expectedVisibility, m.getVisibility());
        Assertions.assertEquals(expectedIsAbstract, m.isAbstract());
        Assertions.assertEquals(expectedIsStatic, m.isStatic());
        Assertions.assertEquals(expectedName, m.getName());
        Assertions.assertEquals(expectedReturnType, m.getReturnType());
        Assertions.assertEquals(expectedParameters, m.getParameters().size());
    }

    /**
     * Validate the given parameter
     *
     * @param p The parameter to validate
     * @param expectedName The expected name of the parameter
     * @param expectedType The expected type of the parameter
     */
    private void validateParameter(Parameter p, String expectedName, String expectedType){
        Assertions.assertEquals(expectedName, p.getName());
        Assertions.assertEquals(expectedType, p.getType());
    }

    /**
     * Validate the given extension
     *
     * @param extension The extension to validate
     * @param expectedExtension The expected extension
     */
    private void validateExtension(String extension, String expectedExtension){
        Assertions.assertEquals(expectedExtension, extension);
    }

    /**
     * Validate the given interface
     *
     * @param i The interface to validate
     * @param expectedName The expected name of the interface
     * @param expectedGeneric The expected generic of the interface
     * @param expectedStereotype The expected stereotype of the interface
     * @param expectedFields The expected number of fields within the interface
     * @param expectedMethods The expected number of methods within the interface
     * @param expectedExtensions The expected number of extensions within the interface
     */
    private void validateInterface(Interface i, String expectedName, String expectedGeneric,
                               String expectedStereotype, int expectedFields, int expectedMethods, int expectedExtensions){
        Assertions.assertEquals(expectedName, i.getName());
        Assertions.assertEquals(expectedGeneric, i.getGeneric());
        Assertions.assertEquals(expectedStereotype, i.getStereotype());
        Assertions.assertEquals(expectedFields, i.getFields().size());
        Assertions.assertEquals(expectedMethods, i.getMethods().size());
        Assertions.assertEquals(expectedExtensions, i.getExtensions().size());
    }

    /**
     * Validate the given enum
     * @param e The enum to validate
     * @param expectedName The expected name of the enum
     */
    private void validateEnum(Enum e, String expectedName, int expectedEnumValues){
        Assertions.assertEquals(expectedName, e.getName());
        Assertions.assertEquals(expectedEnumValues, e.getValues().size());
    }

    /**
     * Validate the given enum value
     *
     * @param enumValue The enum value to validate
     * @param expectedEnumValue The expected enum value
     */
    private void validateEnumValue(String enumValue, String expectedEnumValue){
        Assertions.assertEquals(expectedEnumValue, enumValue);
    }
}
