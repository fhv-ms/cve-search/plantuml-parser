package at.fhv.plantuml.parser;

import org.antlr.v4.runtime.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * Test the antrl grammar
 */
public class GrammarTest {

    /**
     * Parse the given file and check for errors
     * @param file The file to parse
     */
    @Test
    private void parse(String file) throws IOException {
        InputStream in = getClass().getClassLoader().getResourceAsStream(file);
        ClassDiagramLexer lexer = new ClassDiagramLexer(CharStreams.fromStream(in));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ClassDiagramParser parser = new ClassDiagramParser(tokens);
        parser.classdiagram();
        Assertions.assertEquals(0, parser.getNumberOfSyntaxErrors());
    }

    /**
     * Test for abstract and static fields and methods
     */
    @Test
    public void testAbstractStatic() throws IOException {
        parse("abstract_static.plantuml");
    }

    /**
     * Test for fields
     */
    @Test
    public void testFields() throws IOException {
        parse("fields.plantuml");
    }

    /**
     * Test for generics
     */
    @Test
    public void testGenerics() throws IOException {
        parse("generics.plantuml");
    }

    /**
     * Test for labels
     */
    @Test
    public void testLabels() throws IOException {
        parse("labels.plantuml");
    }

    /**
     * Test for methods
     */
    @Test
    public void testMethods() throws IOException {
        parse("methods.plantuml");
    }

    /**
     * Test for packages
     */
    @Test
    public void testPackages() throws IOException {
        parse("packages.plantuml");
    }

    /**
     * Test for relations
     */
    @Test
    public void testRelations() throws IOException {
        parse("relations.plantuml");
    }

    /**
     * Test for stereotypes
     */
    @Test
    public void testStereotypes() throws IOException {
        parse("stereotypes.plantuml");
    }

    /**
     * Test for types
     */
    @Test
    public void testTypes() throws IOException {
        parse("types.plantuml");
    }
}