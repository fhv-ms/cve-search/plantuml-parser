# plantuml-parser

Parser for PlantUML language written in Antrl.

## Development tools

* IntelliJ
  * ANTLR 4 grammar plugin
  * PlantUML Integration
* Graphviz (required to render PlantUML diagrams)
* Gradle

## Getting started

To start developing the plantuml-parser you need to build the application after you cloned the repository:
```
gradle build
```

# Diagram support

## Class diagrams

Currently only a subset of the class diagram language is supported

### Links

#### Types

Relations between classes are defined using the following symbols :

| Type        | Symbol |
|-------------|--------|
| Extension   | <\|--  |
| Composition | *--    |
| Aggregation | o--    |
| Association | <--    |

It is possible a add a label on the relation, using :, followed by the text of the label.

For cardinality, you can use double-quotes "" on each side of the relation.

```
@startuml
Class01 <|-- Class02
Class03 --|> Class04
Class05 *-- Class06
Class07 --* Class08
Class09 o-- Class10
Class11 --o Class12
Class13 <-- Class14
Class15 --> Class16
@enduml
```

```plantuml
Class01 <|-- Class02
Class03 --|> Class04
Class05 *-- Class06
Class07 --* Class08
Class09 o-- Class10
Class11 --o Class12
Class13 <-- Class14
Class15 --> Class16
```

#### Labels

It is possible a add a label on the relation, using :, followed by the text of the label.

For cardinality, you can use double-quotes "" on each side of the relation.

```
@startuml
Class01 "1" *-- "*" Class02 : contains
Class03 o-- Class04 : aggregation
Class05 --> "1" Class06
Class07 "*" <-- "1" Class08
Class09 "*" <-- "1..3" Class10
Class11 "1..*" <-- "3" Class12
Class13 "1..*" <-- Class14
@enduml
```

```plantuml
Class01 "1" *-- "*" Class02 : contains
Class03 o-- Class04 : aggregation
Class05 --> "1" Class06
Class07 "*" <-- "1" Class08
Class09 "*" <-- "1..3" Class10
Class11 "1..*" <-- "3" Class12
Class13 "1..*" <-- Class14
```

### Methods and Fields

#### Visibility
When you define methods or fields, you can use characters to define the visibility of the corresponding item:

| Symbol      | Visibility |
|-------------|------------|
| -           | Private    |
| #           | Protected  |
| +           | Public     |

#### Methods
```
@startuml
class Dummy {
    - method1() : String
    + method2() : String {arg1 : Foo, arg2 : bar}
    # method3() : at.fhv.Object {arg1 : Bar}
    - method4()
    + method5() : void {arg1 : at.fhv.Object}
    # method6() : List<Object> {arg1 : List<Object>}
    - method7() : Map<Key, Value> { arg1 : Map<Key, Value>}
}
@enduml
```
```plantuml
class Dummy {
    - method1() : String
    + method2() : String {arg1 : Foo, arg2 bar}
    # method3() : at.fhv.Object {arg1 : Bar}
    - method4()
    + method5() : void {arg1 : at.fhv.Object}
    # method6() : List<Object> {arg1 : List<Object>}
    - method7() : Map<Key, Value> { arg1 : Map<Key, Value>}
}
```

#### Fields
```
@startuml
class Dummy {
    - field1 : String
    # field2 : at.fhv.Object
    + field3 : List<Object>
    + field4 : Map<Key, Value>
    + field5 : String[]
    + field6 : List<String[]>
    + field7 : List<List<String>>
    + field8 : Map<Key, List<String>>
}
@enduml
```
```plantuml
class Dummy {
    - field1 : String
    # field2 : at.fhv.Object
    + field3 : List<Object>
    + field4 : Map<Key, Value>
    + field5 : String[]
    + field6 : List<String[]>
    + field7 : List<List<String>>
    + field8 : Map<Key, List<String>>
}
```

#### Abstract and Static

You can define static or abstract methods or fields using the {static} or {abstract} modifier.

```
@startuml
class Dummy {
    - {static} id : String
    + {abstract} methods() : void
}
@enduml
```
```plantuml
class Dummy {
    - {static} id : String
    + {abstract} methods() : void
}
```

### Stereotypes

Stereotypes are defined with the class keyword, << and >>.

```
@startuml
class Object << general >>
Object <|-- ArrayList
@enduml
```
```plantuml
class Object << general >>
Object <|--- ArrayList
```

### Class types
```
@startuml
abstract class AbstractList
interface List
interface Collection

List <|-- AbstractList
Collection <|-- AbstractCollection

Collection <|-- List
AbstractCollection <|-- AbstractList
AbstractList <|-- ArrayList

class ArrayList {
    - elementData : Object[]
    - size() : void
}

enum TimeUnit {
    DAYS
    HOURS
    MINUTES
}
@enduml
```
```plantuml
abstract class AbstractList
interface List
interface Collection

List <|-- AbstractList
Collection <|-- AbstractCollection

Collection <|-- List
AbstractCollection <|-- AbstractList
AbstractList <|-- ArrayList

class ArrayList {
    - elementData : Object[]
    - size() : void
}

enum TimeUnit {
    DAYS
    HOURS
    MINUTES
}
```

### Generics
```
@startuml
class Foo<? extends Element> {
    - size() : int
}
Foo *-- Element
@enduml
```
```plantuml
class Foo<? extends Element> {
    - size() : int
}
Foo *-- Element
```

### Packages

You can define a package using the package keyword. Note that package definitions can be nested.

```
@startuml
package at.fhv.test {
    Object <|-- ArrayList
}

package net.sourceforge.plantuml {
    Object <|-- Demo1
    Demo1 *-- Demo2
}

package at.fhv {
    package entity {
        class Foo
    }
    
    package webservice {
        class Bar
    }
}
@enduml
```
```plantuml
package at.fhv.test {
    Object <|-- ArrayList
}

package net.sourceforge.plantuml {
    Object <|-- Demo1
    Demo1 *-- Demo2
}

package at.fhv {
    package entity {
        class Foo
    }
    
    package webservice {
        class Bar
    }
}
```